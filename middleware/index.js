var Campground = require("../models/campground"),
Comment = require("../models/comment");


module.exports = {
    isLoggedIn: function (req, res, next) {

        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect("/login?errorMessage=You need to be loged in to do that!!!");
    },

    isAuthorizedComment: function (req, res, next) {

        Comment.findById(req.params.id, function (err, comment) {
            if (err || !comment.author.id.equals(req.user._id)) {
                res.redirect("back");
            } else {
                req.params.foundComment = comment;
                next();
            }
        })

    },

    isAuthorizedCampground: function (req, res, next) {

        Campground.findById(req.params.id, function (err, campground) {
            if (err || !campground.author.id.equals(req.user._id)) {
                res.redirect("back");
            } else {
                req.params.foundCampground = campground;
                next();
            }
        })

    }
};