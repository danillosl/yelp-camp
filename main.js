var express = require("express"),
    bodyParser = require("body-parser"),
    app = express(),
    mongoose = require("mongoose"),
    passport = require("passport"),
    localStrategy = require("passport-local"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment"),
    User = require("./models/user"),
    methodOverride = require("method-override");
seedDB = require("./seeds");

var campgroundRoutes = require("./routes/campground"),
    commentRoutes = require("./routes/comment"),
    indexRoutes = require("./routes/index");

// seedDB();


app.use(require("express-session")({
    secret: "albert wesker",
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

mongoose.Promise = global.Promise;

app.use(express.static(__dirname + "/public"));

app.use(methodOverride("_method"));

mongoose.connect("mongodb://localhost/yelp_camp", { useMongoClient: true });

app.use(bodyParser.urlencoded({ extended: true }));

app.set("view engine", "ejs");

app.use(function (req, res, next) {

    res.locals.currentUser = req.query.errorMessage;
    res.locals.successMessage = req.query.successMessage;
    res.locals.errorMessage = req.query.errorMessage;
    next();
});

app.use(campgroundRoutes);
app.use(commentRoutes);
app.use(indexRoutes);

app.listen("3000", function () {
    console.log("yelpcamp listening on port 3000");
});