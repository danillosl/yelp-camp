var mongoose = require("mongoose"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment");


var campgrounds = [
    {
        name: "police station",
        image: "http://selectgame.gamehall.uol.com.br/wp-content/uploads/2014/08/Resident-Evil-Raccoon-City-RPD.png",
        description: "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.",
        comments: []
    },
    {
        name: "Racoon city",
        image: "https://www.residentevil.net/share/chro/image/dictionary/raccoon.jpg",
        description: "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.",
        comments: []
    },
    {
        name: "Umbrella corporation",
        image: "https://www.mybizdaq.com/blog/wp-content/uploads/umbrellacorplogo2.jpg",
        description: "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.",
        comments: []
    },
]

function seedDB() {


    cleanAndAddCampgrounds(addCampgrounds);

}

function addCampgrounds() {
    campgrounds.forEach((campground) => {

        Comment.create(
            { text: "This place is greate, but I wish there was internet connection", author: "homer" },
            (err, comment) => {
                if (err) {
                    console.log(err);
                } else {

                    console.log("added comment with author: " + comment.author);

                    campground.comments.push(comment);
                    Campground.create(campground, (err, campground) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("added a campground: " + campground.name);
                        }
                    });
                }
            }
        )

    })
}

function cleanAndAddCampgrounds(addFunction) {
    Campground.remove({}, (err) => {
        if (err) {
            console.log(err);
        }
        console.log("campgrounds clean!");
        if (addFunction) {
            addFunction();
        }

    })
}

module.exports = seedDB;