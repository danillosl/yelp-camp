var express = require("express");

var authenticationAuthorizationMiddleware = require("../middleware/index");

var Campground = require("../models/campground");

var router = express.Router({ mergeParams: true });

router.get("/campgrounds", function (req, res) {

    Campground.find({}, function (err, campgrounds) {
        if (err) {
            console.log(err);
        } else {
            res.render("campground/index", { campgrounds: campgrounds });
        }
    })

})

router.post("/campgrounds", authenticationAuthorizationMiddleware.isLoggedIn, function (req, res) {

    var newCampground = {
        name: req.body.name,
        image: req.body.image,
        description: req.body.description,
        author: {
            id: req.user._id,
            username: req.user.username
        }
    }

    Campground.create(newCampground, function (err, campground) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    })

})

router.get("/campgrounds/new", authenticationAuthorizationMiddleware.isLoggedIn, function (req, res) {

    res.render("campground/new");

})

router.get("/campgrounds/:id", function (req, res) {

    Campground.findById(req.params.id).populate("comments").exec(function (err, campground) {
        if (err) {
            console.log(err);
        } else {
            res.render("campground/show", { campground: campground });
        }
    })
})

router.get("/campgrounds/:id/edit", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedCampground, function (req, res) {
    res.render("campground/edit", { campground: req.params.foundCampground });
});

router.put("/campgrounds/:id", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedCampground, function (req, res) {
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function (err, campground) {

        if (err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    })
});

router.delete("/campgrounds/:id", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedCampground, function (req, res) {
    Campground.findByIdAndRemove(req.params.id, function (err, campground) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    })
})

module.exports = router;