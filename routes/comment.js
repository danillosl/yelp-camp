var express = require("express");

var authenticationAuthorizationMiddleware = require("../middleware/index");

var Campground = require("../models/campground"),
    Comment = require("../models/comment");

var router = express.Router({ mergeParams: true });

router.get("/campgrounds/:id/comments/new", authenticationAuthorizationMiddleware.isLoggedIn, function (req, res) {

    Campground.findById(req.params.id, (err, campground) => {

        if (err) {
            console.log(err);
        } else {
            res.render("comment/new", { campground: campground });
        }
    })
})

router.post("/campgrounds/:id/comments", authenticationAuthorizationMiddleware.isLoggedIn, function (req, res) {
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err);
            res.redirect("/campgrounds");
        } else {


            req.body.comment.author = { id: req.user._id, username: req.user.username };

            Comment.create(req.body.comment, function (err, comment) {
                if (err) {
                    console.log(err);
                } else {


                    campground.comments.push(comment);
                    campground.save();
                    res.redirect("/campgrounds/" + campground._id);
                }
            })
        }
    })
})

router.get("/campgrounds/:campground_id/comments/:id/edit", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedComment, function (req, res) {

    Comment.findById(req.params.id, function (err, comment) {
        if (err) {
            res.redirect("back");
        } else {
            res.render("comment/edit", { campground_id: req.params.campground_id, comment: comment });
        }
    });
});

router.put("/campgrounds/:campground_id/comments/:id", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedComment, function (req, res) {
    Comment.findByIdAndUpdate(req.params.id, req.body.comment, function (err, comment) {
        if (err) {
            res.redirect("back");
        } else {
            res.redirect("/campgrounds/" + req.params.campground_id);
        }
    });
});

router.delete("/campgrounds/:campground_id/comments/:id", authenticationAuthorizationMiddleware.isLoggedIn, authenticationAuthorizationMiddleware.isAuthorizedComment, function (req, res) {
    Comment.findByIdAndRemove(req.params.id, function (err, campground) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds/" + req.params.campground_id);
        }
    })
})




module.exports = router;